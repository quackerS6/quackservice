﻿using MassTransit;
using QuackService.Dal;
using QuackService.Dal.Entities;
using Shared.Messages;
using System.Threading.Tasks;

namespace QuackService.Messaging {
    public class ImageUpdatedConsumer : IConsumer<ImageUploadMessage> {

        private readonly IUnitOfWork _unitOfWork;

        public ImageUpdatedConsumer(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }

        public async Task Consume(ConsumeContext<ImageUploadMessage> context) {
            UserEntity entity = new UserEntity() {
                Id = context.Message.UserId,
                ImageUrl = context.Message.Url
            };
            using (_unitOfWork) {
                await _unitOfWork.UserRepository.UpdateUser(entity);
                await _unitOfWork.Complete();
            }
        }
    }
}
