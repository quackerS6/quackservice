﻿using MassTransit;
using QuackService.Dal;
using QuackService.Dal.Entities;
using Shared.Messages;
using System.Threading.Tasks;

namespace QuackService.Messaging {
    public class UserCreatedConsumer : IConsumer<UserCreatedMessage> {

        private readonly IUnitOfWork _unitOfWork;

        public UserCreatedConsumer(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }

        public async Task Consume(ConsumeContext<UserCreatedMessage> context) {
            UserEntity entity = new UserEntity() {
                Id = context.Message.Id
            };
            using (_unitOfWork) {
                _unitOfWork.UserRepository.Add(entity);
                await _unitOfWork.Complete();
            }
        }
    }
}
