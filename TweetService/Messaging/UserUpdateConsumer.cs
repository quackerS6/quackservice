﻿using MassTransit;
using QuackService.Dal;
using QuackService.Dal.Entities;
using Shared.Messages;
using System.Threading.Tasks;

namespace QuackService.Messaging {
    public class UserUpdateConsumer : IConsumer<UserUpdateMessage> {

        private readonly IUnitOfWork _unitOfWork;

        public UserUpdateConsumer(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }

        public async Task Consume(ConsumeContext<UserUpdateMessage> context) {
            UserUpdateMessage message = context.Message;
            UserEntity entity = new UserEntity() {
                Name = message.Firstname,
                Id = message.Id,
                Tag = message.Tag
            };
            using (_unitOfWork) {
                await _unitOfWork.UserRepository.UpdateUser(entity);
                await _unitOfWork.Complete();
            }
        }
    }
}
