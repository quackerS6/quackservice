﻿using AutoMapper;
using QuackService.Dal.Entities;
using QuackService.Models;
using QuackService.Models.Responses;

namespace QuackService.Config {
    public class QuackMappingProfile : Profile {

        public QuackMappingProfile() {

            // Request
            CreateMap<QuackModel, QuackEntity>();
            CreateMap<string, HashtagEntity>().ForMember(dest => dest.HashTag, opts => opts.MapFrom(src => src));
            CreateMap<string, MentionEntity>().ForMember(dest => dest.UserTag, opts => opts.MapFrom(src => src));

            // Response
            CreateMap<QuackEntity, QuackResponse>().ForMember(
                destination => destination.UserInfo,
                options => options.MapFrom(source => source.User)
            );
        }

    }
}
