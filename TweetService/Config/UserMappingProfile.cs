﻿using AutoMapper;
using QuackService.Dal.Entities;
using QuackService.Models;
using QuackService.Models.Responses;
using System.Linq;

namespace QuackService.Config {
    public class UserMappingProfile : Profile {

        public UserMappingProfile() {
            CreateMap<UserEntity, UserResponse>()
                .ForMember(dest => dest.Followers, options => {
                    options.MapFrom(src => src.Followers.Select(x => x.Follower));
                })
                .ForMember(dest => dest.Following, options => {
                    options.MapFrom(src => src.Following.Select(x => x.User));
                })
                .ForMember(dest => dest.Mentions, options => {
                    options.MapFrom(src => src.Mentions.Select(x => x.Quack));
                });

            CreateMap<UserUpdateModel, UserEntity>();
            CreateMap<UserEntity, UserInfoInQuack>();
            CreateMap<FollowEntity, UserResponse>();

        }
    }
}
