﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;

namespace QuackService.Utils {
    public static class StartupExtentions {
        public static IConfigurationBuilder SetDevSettingsIfNotInDocker(this IConfigurationBuilder builder, IWebHostEnvironment env) {
            if (!env.IsInDocker() && env.IsDevelopment()) {
                builder.AddJsonFile("appsettings.NoDockerDev.json");
            }
            return builder;
        }

        public static bool IsInDocker(this IWebHostEnvironment env) {
            return bool.Parse(Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER") ?? bool.FalseString);
        }
    }
}
