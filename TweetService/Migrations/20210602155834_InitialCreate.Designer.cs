﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using QuackService.Dal;

namespace QuackService.Migrations
{
    [DbContext(typeof(QuackAppContext))]
    [Migration("20210602155834_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.4")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("QuackService.Dal.Entities.FollowEntity", b =>
                {
                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("FollowerId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("FollowingSince")
                        .HasColumnType("datetime2");

                    b.HasKey("UserId", "FollowerId");

                    b.HasIndex("FollowerId");

                    b.ToTable("followers");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.HashtagEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("HashTag")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("QuackId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("QuackId");

                    b.ToTable("Hashtags");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.MentionEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("QuackId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("UserTag")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("QuackId");

                    b.HasIndex("UserId");

                    b.ToTable("Mentions");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.QuackEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("PostDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Text")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("quacks");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.UserEntity", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("ImageUrl")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Tag")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("users");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.FollowEntity", b =>
                {
                    b.HasOne("QuackService.Dal.Entities.UserEntity", "Follower")
                        .WithMany("Following")
                        .HasForeignKey("FollowerId")
                        .OnDelete(DeleteBehavior.NoAction)
                        .IsRequired();

                    b.HasOne("QuackService.Dal.Entities.UserEntity", "User")
                        .WithMany("Followers")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.NoAction)
                        .IsRequired();

                    b.Navigation("Follower");

                    b.Navigation("User");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.HashtagEntity", b =>
                {
                    b.HasOne("QuackService.Dal.Entities.QuackEntity", "Quack")
                        .WithMany("Hashtags")
                        .HasForeignKey("QuackId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Quack");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.MentionEntity", b =>
                {
                    b.HasOne("QuackService.Dal.Entities.QuackEntity", "Quack")
                        .WithMany("Mentions")
                        .HasForeignKey("QuackId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("QuackService.Dal.Entities.UserEntity", "User")
                        .WithMany("Mentions")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.NoAction)
                        .IsRequired();

                    b.Navigation("Quack");

                    b.Navigation("User");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.QuackEntity", b =>
                {
                    b.HasOne("QuackService.Dal.Entities.UserEntity", "User")
                        .WithMany("Quacks")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.QuackEntity", b =>
                {
                    b.Navigation("Hashtags");

                    b.Navigation("Mentions");
                });

            modelBuilder.Entity("QuackService.Dal.Entities.UserEntity", b =>
                {
                    b.Navigation("Followers");

                    b.Navigation("Following");

                    b.Navigation("Mentions");

                    b.Navigation("Quacks");
                });
#pragma warning restore 612, 618
        }
    }
}
