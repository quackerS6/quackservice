using AutoMapper;
using GreenPipes;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using QuackService.Config;
using QuackService.Dal;
using QuackService.Dal.Entities;
using QuackService.Dal.Includers;
using QuackService.Dal.Includers.Includers;
using QuackService.Messaging;
using QuackService.Models.Responses;
using QuackService.Utils;
using System;
using System.Linq;

namespace QuackService {
    public class Startup {

        public IConfiguration Configuration { get; }
        private IWebHostEnvironment _env;

        public Startup(IWebHostEnvironment env) {
            _env = env;
            Configuration = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"secrets/appsettings.{env.EnvironmentName}.json", optional: true)
            .SetDevSettingsIfNotInDocker(env)
            .AddEnvironmentVariables()
            .Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddControllers().AddNewtonsoftJson(options => {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            // Automapper
            var mapperConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new QuackMappingProfile());
                mc.AddProfile(new UserMappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            // Database
            services.AddDbContext<QuackAppContext>(options => {
                options.UseSqlServer(Configuration.GetConnectionString("quackDB"));
            });

            // Includers
            services.AddScoped<ICollectionIncluder<IQueryable<UserEntity>, object>, UserEntityIncluder>();
            services.AddScoped<ICollectionIncluder<UserResponse, Guid>, UserReponseIncluder>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Message broker
            services.AddMassTransit(ms => {
                ms.AddConsumer<UserUpdateConsumer>();
                ms.AddConsumer<UserCreatedConsumer>();
                ms.AddConsumer<ImageUpdatedConsumer>();
                ms.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg => {
                    cfg.UseHealthCheck(provider);
                    cfg.Host(new Uri(Configuration.GetSection("RabbitMQ")["host"]), host => {
                        host.Username(Configuration.GetSection("RabbitMQ")["username"]);
                        host.Password(Configuration.GetSection("RabbitMQ")["password"]);
                    });
                    cfg.ReceiveEndpoint("quackqueue", endpoint => {
                        endpoint.PrefetchCount = 16;
                        endpoint.UseMessageRetry(retry => retry.Interval(2, 100));
                        endpoint.ConfigureConsumer<UserCreatedConsumer>(provider);
                        endpoint.ConfigureConsumer<UserUpdateConsumer>(provider);
                        endpoint.ConfigureConsumer<ImageUpdatedConsumer>(provider);
                    });
                }));
            });


            services.AddMassTransitHostedService();

            // CORS
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            DoMigrations(app);
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }

        private void DoMigrations(IApplicationBuilder app) {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<QuackAppContext>();
            context.Database.Migrate();
        }

    }
}
