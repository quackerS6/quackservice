﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using QuackService.Dal;
using QuackService.Dal.Entities;
using QuackService.Dal.Entities.ModifiedEntities;
using QuackService.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuackService.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class QuacksController : ControllerBase {

        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public QuacksController(IMapper mapper, IUnitOfWork unitOfWork) {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        // POST api/Quacks
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] QuackModel quack, [FromHeader] Guid userId) {
            IActionResult result;
            ValidateQuackContent(quack);
            if (!ModelState.IsValid) {
                result = BadRequest(ModelState);
            }
            else {
                using (_unitOfWork) {
                    QuackEntity entity = _mapper.Map<QuackEntity>(quack);
                    foreach (MentionEntity mention in entity.Mentions) {
                        UserEntity taggedUser = await _unitOfWork.UserRepository.GetUserByTag(mention.UserTag);
                        if (taggedUser == null) {
                            return BadRequest(mention);
                        }
                        taggedUser.Mentions = new List<MentionEntity>() { mention };
                    }
                    entity.UserId = userId;
                    _unitOfWork.QuackRepository.Add(entity);
                    result = Created($"/quacks/{entity.Id}", entity);
                    await _unitOfWork.Complete();
                }
            }

            return result;
        }

        [HttpGet("trends")]
        public async Task<IActionResult> GetTrends() {
            using (_unitOfWork) {
                IEnumerable<HashtagTrend> trends = await _unitOfWork.HashtagRepository.GetTrendingHashtags();
                return Ok(trends);
            }
        }

        private void ValidateQuackContent(QuackModel quack) {
            VerifyTokensInQuack(quack.Text, quack.HashTags, '#');
            VerifyTokensInQuack(quack.Text, quack.Mentions, '@');
        }

        private void VerifyTokensInQuack(string quackText, string[] toCheck, char prefix) {
            if (toCheck.Length > 5) {
                // A quack cannot have more than 5 hashtags or mentions
                ModelState.AddModelError("TooManyTokens", $"The quack has too many tokens prefixed with {prefix}");
            }
            foreach (string item in toCheck) {
                string prefixedItem = prefix + item;
                if (!(quackText.StartsWith(prefixedItem) ||
                    quackText.EndsWith(" " + prefixedItem) ||
                    quackText.Contains(" " + prefixedItem))) {
                    ModelState.AddModelError("InvalidToken", $"The Quack does not contain the token {prefixedItem}");
                    break;
                }

            }
        }
    }
}
