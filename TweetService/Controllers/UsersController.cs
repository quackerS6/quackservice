﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using QuackService.Dal;
using QuackService.Dal.Entities;
using QuackService.Dal.Includers;
using QuackService.Models.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuackService.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase {

        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICollectionIncluder<UserResponse, Guid> _includer;

        public UsersController(IMapper mapper, IUnitOfWork unitOfWork, ICollectionIncluder<UserResponse, Guid> includer) {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _includer = includer;
        }


        // GET api/users/<Tag>
        [HttpGet("{tagOrId}")]
        public async Task<IActionResult> GetByTagOrId([FromRoute] string tagOrId, [FromQuery] bool isTag, [FromQuery] Guid userId, [FromQuery] string[] include) {
            IActionResult response = NoContent();
            UserEntity entity;

            using (_unitOfWork) {
                if (isTag) {
                    entity = await _unitOfWork.UserRepository.GetUserByTag(tagOrId, include);
                }
                else if (Guid.TryParse(tagOrId, out Guid id)) {
                    entity = await _unitOfWork.UserRepository.GetUserById(id, include);
                }
                else {
                    return BadRequest(tagOrId);
                }

                if (entity != null) {
                    UserResponse responseModel = _mapper.Map<UserResponse>(entity);
                    _includer.PrepValue = entity.Id;
                    await _includer.IncludeOnEntity(responseModel, include);
                    if (userId != Guid.Empty) {
                        responseModel.IsFollowing = await _unitOfWork.FollowerRepository.IsFollowing(userId, entity.Id);
                    }
                    response = Ok(responseModel);
                }
                return response;
            }
        }

        [HttpGet("{userId}/recommended")]
        public async Task<IActionResult> GetRecommendedToFollow([FromRoute] Guid userId) {
            using (_unitOfWork) {
                IEnumerable<UserEntity> recommendedEntities = await _unitOfWork.UserRepository.GetRandomUsersToFollow(userId);
                var response = _mapper.Map<IEnumerable<UserEntity>, IEnumerable<UserResponse>>(recommendedEntities);
                return Ok(response);
            }
        }

        // GET /api/users/tag/<tagname>
        [HttpGet("tag/{tagname}")]
        public async Task<IActionResult> GetTagByName([FromRoute] string tagname) {
            ActionResult result;

            using (_unitOfWork) {
                UserEntity entity = await _unitOfWork.UserRepository.GetUserByTag(tagname);

                if (entity != null) {
                    TagResponse response = new TagResponse() {
                        Tagname = tagname
                    };
                    result = Ok(response);
                }
                else {
                    result = NoContent();
                }

                return result;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteById([FromRoute] Guid id) {
            using (_unitOfWork) {
                await _unitOfWork.UserRepository.DeleteUser(id);
                await _unitOfWork.Complete();
            }
            return Ok();
        }

    }
}
