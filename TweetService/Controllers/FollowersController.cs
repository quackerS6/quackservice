﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using QuackService.Dal;
using QuackService.Dal.Entities;
using System;
using System.Threading.Tasks;

namespace QuackService.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class FollowersController : ControllerBase {

        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly int _followerPageSize = 16;

        public FollowersController(IMapper mapper, IUnitOfWork unitOfWork) {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        // POST /api/followers/userIdToFollow
        [HttpPost("{userIdToFollow}")]
        public async Task<IActionResult> Follow([FromRoute] Guid userIdToFollow, [FromHeader] Guid userId) {
            using (_unitOfWork) {
                FollowEntity entity = new FollowEntity {
                    FollowerId = userId,
                    UserId = userIdToFollow
                };
                _unitOfWork.FollowerRepository.Add(entity);
                await _unitOfWork.Complete();
                return Created($"/api/users/{entity.UserId}", entity);
            }
        }

        // POST /api/followers/userIdToUnfollow
        [HttpDelete("{userIdToUnfollow}")]
        public async Task<IActionResult> Unfollow([FromRoute] Guid userIdToUnfollow, [FromHeader] Guid userId) {
            using (_unitOfWork) {
                await _unitOfWork.FollowerRepository.DeleteFollow(userId, userIdToUnfollow);
                await _unitOfWork.Complete();
                return NoContent();
            }
        }

    }
}
