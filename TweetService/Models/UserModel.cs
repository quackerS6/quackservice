﻿using System;

namespace QuackService.Models {
    public class UserModel {
        public Guid Id { get; set; }
        public string Tag { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}
