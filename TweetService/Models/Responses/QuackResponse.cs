﻿using System;

namespace QuackService.Models.Responses {
    public class QuackResponse {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public DateTime PostDate { get; set; }
        public UserInfoInQuack UserInfo { get; set; }
    }
}
