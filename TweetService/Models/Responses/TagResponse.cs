﻿namespace QuackService.Models.Responses {
    public class TagResponse {
        public string Tagname { get; set; }
    }
}
