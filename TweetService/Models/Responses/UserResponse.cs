﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuackService.Models.Responses {
    public class UserResponse {

        private IList<QuackResponse> _quacks = new List<QuackResponse>();
        public Guid Id { get; set; }
        public string Tag { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsFollowing { get; set; }
        public int QuackCount { get; set; }
        public int FollowingCount { get; set; }
        public int FollowerCount { get; set; }
        // Todo: make IEnumerable instead
        public ICollection<QuackResponse> QuacksOnTimeline { get; set; }
        public IEnumerable<QuackResponse> Mentions { get; set; }
        public IEnumerable<UserResponse> Followers { get; set; }
        public IEnumerable<UserResponse> Following { get; set; }

        // They cannot be ordered by entity framework when including, so that is why it happens here
        public IList<QuackResponse> Quacks {
            get {
                return _quacks;
            }

            set {
                _quacks = value.OrderByDescending(x => x.PostDate).ToList();
            }

        }

    }
}
