﻿namespace QuackService.Models.Responses {
    public class UserInfoInQuack {
        public string Tag { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}
