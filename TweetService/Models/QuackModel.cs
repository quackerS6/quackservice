﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuackService.Models {
    public class QuackModel {
        [Required]
        [StringLength(140)]
        public string Text { get; set; }
        // Todo: Think about putting this logic in the database
        public DateTime PostDate { get; set; } = DateTime.Now;
        public string[] HashTags { get; set; }
        public string[] Mentions { get; set; }
    }
}
