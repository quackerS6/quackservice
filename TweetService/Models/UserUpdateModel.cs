﻿namespace QuackService.Models {
    public class UserUpdateModel {
        public string Tag { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}
