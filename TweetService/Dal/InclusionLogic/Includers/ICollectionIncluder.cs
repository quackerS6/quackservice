﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuackService.Dal.Includers {
    public interface ICollectionIncluder<TEntity, TValue> {
        Task<TEntity> IncludeOnEntity(TEntity entity, IEnumerable<string> toInclude);
        TValue PrepValue { get; set; }
    }
}
