﻿using QuackService.Dal.Entities;
using QuackService.Dal.InclusionLogic.Appenders;
using QuackService.Dal.InclusionLogic.Appenders.UserAppenders;
using System.Collections.Generic;
using System.Linq;

namespace QuackService.Dal.Includers.Includers {
    public class UserEntityIncluder : BaseCollectionIncluder<IQueryable<UserEntity>, object>, ICollectionIncluder<IQueryable<UserEntity>, object> {
        public UserEntityIncluder(QuackAppContext context) : base(context) {
            EntityAppenders = new List<IEntityAppender<IQueryable<UserEntity>, object>>() {
                new UserFollowerAppender(),
                new UserFollowingAppender(),
                new UserQuacksAppender(),
                new UserMentionAppender(),
                new UserQuacksAppender()
            };
        }
    }
}
