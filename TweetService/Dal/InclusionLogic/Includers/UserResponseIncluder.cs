﻿using AutoMapper;
using QuackService.Dal.InclusionLogic.Appenders.UserAppenders;
using QuackService.Models.Responses;
using System;
using System.Collections.Generic;

namespace QuackService.Dal.Includers.Includers {
    public class UserReponseIncluder : BaseCollectionIncluder<UserResponse, Guid>, ICollectionIncluder<UserResponse, Guid> {

        public UserReponseIncluder(IUnitOfWork unitOfWork, IMapper mapper) : base() {
            EntityAppenders = new List<IEntityAppender<UserResponse, Guid>>() {
                new UserTimelineAppender(unitOfWork, mapper)
            };
        }
    }
}
