﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuackService.Dal.Includers {
    public abstract class BaseCollectionIncluder<TEntity, TValue> {

        public TValue PrepValue { get; set; }
        public TEntity Output { get; private set; }

        protected IEnumerable<IEntityAppender<TEntity, TValue>> EntityAppenders;

        public BaseCollectionIncluder() {
        }

        public BaseCollectionIncluder(TValue prepValue) {
            PrepValue = prepValue;
        }

        public async Task<TEntity> IncludeOnEntity(TEntity entity, IEnumerable<string> toInclude) {
            if (toInclude != null) {
                toInclude = toInclude.Select(x => x.ToLower());
            }
            else {
                toInclude = new string[0];
            }
            Output = entity;
            foreach (var appender in EntityAppenders) {
                appender.Prepare(PrepValue);
                await appender.IncludeIfInRequest(Output, toInclude);
                Output = appender.Entity;
            }
            return Output;
        }
    }
}
