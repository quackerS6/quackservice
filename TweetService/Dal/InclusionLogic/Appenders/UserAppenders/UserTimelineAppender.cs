﻿using AutoMapper;
using QuackService.Dal.Entities;
using QuackService.Dal.Includers;
using QuackService.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuackService.Dal.InclusionLogic.Appenders.UserAppenders {
    public class UserTimelineAppender : IEntityAppender<UserResponse, Guid> {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private Guid _entityId;

        public UserResponse Entity { get; private set; }

        public UserTimelineAppender(IUnitOfWork unitOfWork, IMapper mapper) {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Prepare(Guid value) {
            _entityId = value;
        }

        public async Task IncludeIfInRequest(UserResponse response, IEnumerable<string> toInclude) {
            Entity = response;
            if (toInclude.Contains("timeline")) {
                var quacks = await _unitOfWork.QuackRepository.GetProfileQuacksFromUser(_entityId);
                response.QuacksOnTimeline = _mapper.Map<IEnumerable<QuackEntity>, IEnumerable<QuackResponse>>(quacks).ToList();
            }
        }

    }
}
