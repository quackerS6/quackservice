﻿using Microsoft.EntityFrameworkCore;
using QuackService.Dal.Entities;
using QuackService.Dal.Includers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuackService.Dal.InclusionLogic.Appenders {
    public class UserFollowerAppender : IEntityAppender<IQueryable<UserEntity>, object> {


        public UserFollowerAppender() {
        }

        public IQueryable<UserEntity> Entity { get; private set; }

        public Task IncludeIfInRequest(IQueryable<UserEntity> entity, IEnumerable<string> toInclude) {
            Entity = entity;
            if (toInclude.Contains("followers")) {
                Entity = Entity
                    .Include(x => x.Followers)
                    .ThenInclude(x => x.Follower)
                    .AsSplitQuery();

            }
            return Task.CompletedTask;
        }

        public void Prepare(object value) {

        }

    }
}
