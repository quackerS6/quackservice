﻿using Microsoft.EntityFrameworkCore;
using QuackService.Dal.Entities;
using QuackService.Dal.Includers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuackService.Dal.InclusionLogic.Appenders {
    public class UserFollowingAppender : IEntityAppender<IQueryable<UserEntity>, object> {

        public IQueryable<UserEntity> Entity { get; private set; }

        public void Prepare(object value) {
        }

        public Task IncludeIfInRequest(IQueryable<UserEntity> entity, IEnumerable<string> toInclude) {
            Entity = entity;
            if (toInclude.Contains("following")) {
                Entity = Entity
                    .Include(x => x.Following)
                    .ThenInclude(x => x.User);
            }
            return Task.CompletedTask;

        }
    }
}
