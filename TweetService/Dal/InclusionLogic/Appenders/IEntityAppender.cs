﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuackService.Dal.Includers {
    public interface IEntityAppender<TEntity, TValue> {

        Task IncludeIfInRequest(TEntity entity, IEnumerable<string> toInclude);
        void Prepare(TValue value);
        TEntity Entity { get; }
    }
}
