﻿using System;

namespace QuackService.Dal.Entities {
    public class HashtagEntity {
        public Guid Id { get; set; }
        public string HashTag { get; set; }

        public Guid QuackId { get; set; }
        public QuackEntity Quack { get; set; }
    }
}
