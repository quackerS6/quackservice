﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuackService.Dal.Entities {
    [Table("users")]
    public class UserEntity {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        public string Tag { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }

        // One to many
        [NotMapped]
        public int QuackCount { get; set; }

        public ICollection<QuackEntity> Quacks { get; set; }

        // Following system (many to many)
        [NotMapped]
        public int FollowingCount { get; set; }
        public ICollection<FollowEntity> Following { get; set; }

        [NotMapped]
        public int FollowerCount { get; set; }
        public ICollection<FollowEntity> Followers { get; set; }

        [ForeignKey("QuackId")]
        public virtual ICollection<MentionEntity> Mentions { get; set; }
    }
}
