﻿using System;

namespace QuackService.Dal.Entities {
    public class MentionEntity {
        public Guid Id { get; set; }
        public string UserTag { get; set; }

        public Guid QuackId { get; set; }
        public QuackEntity Quack { get; set; }
        public Guid UserId { get; set; }
        public UserEntity User { get; set; }

    }
}
