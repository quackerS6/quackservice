﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuackService.Dal.Entities {
    [Table("quacks")]
    public class QuackEntity {
        public Guid Id { get; set; }
        public string Text { get; set; }

        // User
        public Guid UserId { get; set; }
        public UserEntity User { get; set; }

        // Mentions
        [ForeignKey("QuackId")]
        public ICollection<MentionEntity> Mentions { get; set; }

        // Hashtags
        [ForeignKey("QuackId")]
        public ICollection<HashtagEntity> Hashtags { get; set; }

        [DataType(DataType.DateTime)]
        [Column(TypeName = "datetime2")]
        public DateTime PostDate { get; set; }

        // Like count / Likes / IsLiked by the person receiving the tweet
        // IsEdited
        // EditedDate
    }
}
