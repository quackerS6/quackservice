﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuackService.Dal.Entities {
    [Table("followers")]
    public class FollowEntity {
        public Guid FollowerId { get; set; }
        public UserEntity Follower { get; set; }
        public Guid UserId { get; set; }
        public UserEntity User { get; set; }

        [DataType(DataType.DateTime)]
        [Column(TypeName = "datetime2")]
        public DateTime FollowingSince { get; set; } = DateTime.Now;
    }
}
