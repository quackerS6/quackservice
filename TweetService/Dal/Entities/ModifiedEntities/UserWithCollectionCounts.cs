﻿namespace QuackService.Dal.Entities.ModifiedEntities {
    public class UserWithCollectionCounts {
        public UserEntity User { get; set; }
        public int QuackCount { get; set; }
        public int FollowingCount { get; set; }
        public int FollowerCount { get; set; }

        public UserEntity GetUser() {
            User.QuackCount = QuackCount;
            User.FollowerCount = FollowerCount;
            User.FollowingCount = FollowingCount;
            return User;
        }
    }
}
