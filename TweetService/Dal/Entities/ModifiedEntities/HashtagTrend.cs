﻿namespace QuackService.Dal.Entities.ModifiedEntities {
    public class HashtagTrend {
        public string Hashtag { get; set; }
        public int TimesQuacked { get; set; }
    }
}
