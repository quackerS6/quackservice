﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using QuackService.Dal.Entities;
using QuackService.Dal.Entities.ModifiedEntities;
using QuackService.Dal.Includers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {
    public class UserRepository : GenericRepository<UserEntity>, IUserRepository {

        private readonly QuackAppContext _context;
        private readonly ICollectionIncluder<IQueryable<UserEntity>, object> _includer;
        private readonly Random _random;

        public UserRepository(QuackAppContext context, ICollectionIncluder<IQueryable<UserEntity>, object> includer) : base(context) {
            _includer = includer;
            _context = context;
            _random = new Random();
        }

        public async Task<UserEntity> GetUserByTag(string tagname, IEnumerable<string> toInclude = null) {
            var userWithCountContext = await GetContextWithUserCountSelected(toInclude);
            UserWithCollectionCounts userWithCount = await userWithCountContext.FirstOrDefaultAsync(x => x.User.Tag == tagname);
            UserEntity user = userWithCount?.GetUser();
            ;
            return user;
        }

        public async Task<UserEntity> GetUserById(Guid id, IEnumerable<string> toInclude = null) {
            var userWithCountContext = await GetContextWithUserCountSelected(toInclude);
            UserWithCollectionCounts userWithCount = await userWithCountContext.FirstOrDefaultAsync(x => x.User.Id == id);
            UserEntity user = userWithCount?.GetUser();
            return user;
        }

        public async Task<UserEntity> UpdateUser(UserEntity updatedData) {
            UserEntity toUpdate = await _context.Users.FirstOrDefaultAsync(x => x.Id == updatedData.Id);
            if (toUpdate == null) {
                return null;
            }
            else {
                UpdateEntity(updatedData, toUpdate);
                return toUpdate;
            }
        }

        public async Task DeleteUser(Guid id) {
            UserEntity toDelete = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
            // Todo: DELETE followers
            // Todo: DELETE from auth0
            _context.Remove(toDelete);
        }

        /// <param name="userId">The id of the user to recommend to</param>
        public async Task<IEnumerable<UserEntity>> GetRandomUsersToFollow(Guid userId) {
            UserEntity recommendSubject = await GetUserById(userId);
            // Count is reduced by the users who the user is following, by itself, and by the amount to take
            int skipCount = (int)(_random.NextDouble() * (_context.Users.Count() - (recommendSubject.FollowingCount + 4)));
            skipCount = Math.Max(0, skipCount);
            var userIdParam = new SqlParameter("userId", userId);
            return await _context.Users
                .FromSqlRaw("SELECT * FROM users WHERE id != @userId AND id NOT IN (SELECT UserId from followers WHERE FollowerId = @userId)", userIdParam)
                .OrderBy(x => x.Id)
                .Skip(skipCount)
                .Take(3)
                .ToListAsync();
        }

        private void UpdateEntity(UserEntity source, UserEntity destination) {
            destination.ImageUrl = source.ImageUrl ?? destination.ImageUrl;
            destination.Name = source.Name ?? destination.Name;
            destination.Tag = source.Tag ?? destination.Tag;
        }

        private async Task<IQueryable<UserWithCollectionCounts>> GetContextWithUserCountSelected(IEnumerable<string> toInclude) {
            IQueryable<UserEntity> queryableUser = _context.Users.AsQueryable(); //.Include(x => x.Mentions).ThenInclude(x => x.Quack);
            queryableUser = await _includer.IncludeOnEntity(queryableUser, toInclude);

            return queryableUser
                .Select(user => new UserWithCollectionCounts {
                    User = user,
                    FollowerCount = user.Followers.Count(),
                    FollowingCount = user.Following.Count(),
                    QuackCount = user.Quacks.Count()
                });
        }

    }
}
