﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {
    public interface IGenericRepository<TEntity> {

        /// <summary>
        /// Adds a new single entity to the database
        /// </summary>
        /// <param name="entity">The entity to add</param>
        void Add(TEntity entity);

        /// <summary>
        /// Adds multiple new entities to the database
        /// </summary>
        /// <param name="entities">The entities to add</param>
        void AddRange(IEnumerable<TEntity> entities);
        Task<TEntity> Get(Guid id);
        /// <summary>
        /// Gets all entities of the associated table
        /// </summary>
        /// <returns>All entities in the associated table</returns>
        Task<IEnumerable<TEntity>> GetAll();

        /// <summary>
        /// Finds entities by the given lambda expression
        /// </summary>
        /// <param name="predicate">The lambda for filtering the entities</param>
        /// <returns>The found entities. An empty collection if none are found.</returns>
        Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Deletes the given entity from the database
        /// </summary>
        /// <param name="entity">The entity to deletes</param>
        void Remove(TEntity entity);
    }
}