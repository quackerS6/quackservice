﻿using QuackService.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {
    public interface IUserRepository : IGenericRepository<UserEntity> {

        Task<UserEntity> GetUserByTag(string tagname, IEnumerable<string> toInclude = null);
        Task<UserEntity> GetUserById(Guid id, IEnumerable<string> toInclude = null);
        Task<IEnumerable<UserEntity>> GetRandomUsersToFollow(Guid userId);
        Task<UserEntity> UpdateUser(UserEntity entity);
        Task DeleteUser(Guid id);
    }
}
