﻿using QuackService.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {
    public interface IQuackRepository : IGenericRepository<QuackEntity> {
        Task<IEnumerable<QuackEntity>> GetProfileQuacksFromUser(Guid userId);
        Task<IEnumerable<QuackEntity>> GetTweetsAndRetweetsFromUser(Guid userId);
    }
}
