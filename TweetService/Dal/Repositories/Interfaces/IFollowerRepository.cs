﻿using QuackService.Dal.Entities;
using System;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {
    public interface IFollowerRepository : IGenericRepository<FollowEntity> {
        Task DeleteFollow(Guid followerId, Guid userId);
        Task<bool> IsFollowing(Guid followerId, Guid userId);
    }
}