﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class {

        protected readonly DbContext Context;

        public GenericRepository(DbContext context) {
            Context = context;
        }
        public void Add(TEntity entity) {
            Context.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities) {
            Context.AddRange(entities);
        }

        public async Task<TEntity> Get(Guid id) {
            return await Context.Set<TEntity>().FindAsync(id);
        }

        public async Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate) {
            return await Context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAll() {
            return await Context.Set<TEntity>().ToListAsync();
        }

        public void Remove(TEntity entity) {
            Context.Set<TEntity>().Remove(entity);
        }
    }
}
