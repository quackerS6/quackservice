﻿using Microsoft.EntityFrameworkCore;
using QuackService.Dal.Entities.ModifiedEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {

    public class HashtagRepository : IHashtagRepository {
        private readonly QuackAppContext _context;

        public HashtagRepository(QuackAppContext context) {
            _context = context;
        }

        public async Task<IEnumerable<HashtagTrend>> GetTrendingHashtags(int amount = 5, int sinceDaysAgo = 7) {
            return await _context.Hashtags
                .Where(x => x.Quack.PostDate >= DateTime.Now.AddDays(-sinceDaysAgo))
                .GroupBy(x => x.HashTag)
                .Select(x => new HashtagTrend {
                    Hashtag = x.Key,
                    TimesQuacked = x.Count()
                })
                .OrderByDescending(x => x.TimesQuacked)
                .Take(amount)
                .ToListAsync();
        }
    }
}
