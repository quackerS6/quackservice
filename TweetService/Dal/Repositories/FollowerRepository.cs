﻿using Microsoft.EntityFrameworkCore;
using QuackService.Dal.Entities;
using System;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {
    public class FollowerRepository : GenericRepository<FollowEntity>, IFollowerRepository {

        private readonly QuackAppContext _context;

        public FollowerRepository(QuackAppContext context) : base(context) {
            _context = context;
        }

        public async Task DeleteFollow(Guid followerId, Guid userId) {
            FollowEntity toUnfollow = await _context.Followers.FirstOrDefaultAsync(x => x.FollowerId == followerId && x.UserId == userId);
            if (toUnfollow != null) {
                _context.Remove(toUnfollow);
            }
        }

        public async Task<bool> IsFollowing(Guid followerId, Guid userId) {
            FollowEntity entity = await _context.Followers.FirstOrDefaultAsync(x => x.FollowerId == followerId && x.UserId == userId);
            return entity != null;
        }
    }
}
