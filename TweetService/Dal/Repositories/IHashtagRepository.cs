﻿using QuackService.Dal.Entities.ModifiedEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {
    public interface IHashtagRepository {
        Task<IEnumerable<HashtagTrend>> GetTrendingHashtags(int amount = 5, int sinceDaysAgo = 7);
    }
}