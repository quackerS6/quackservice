﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using QuackService.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuackService.Dal.Repositories {
    public class QuackRepository : GenericRepository<QuackEntity>, IQuackRepository {

        private readonly QuackAppContext _context;

        public QuackRepository(QuackAppContext context) : base(context) {
            _context = context;
        }

        public async Task<IEnumerable<QuackEntity>> GetProfileQuacksFromUser(Guid userId) {
            var userIdParam = new SqlParameter("userId", userId);
            return await _context.Quacks
                //.FromSqlRaw("SELECT quacks.*, users.Name, users.Tag, users.ImageUrl FROM quacks JOIN users ON users.Id = @userId WHERE userId = @userId OR userId IN (SELECT UserId from followers WHERE FollowerId = @userId) ORDER BY PostDate DESC", userIdParam)
                .FromSqlRaw("SELECT * FROM quacks WHERE userId = @userId OR userId IN (SELECT UserId from followers WHERE FollowerId = @userId)", userIdParam)
                .Include(x => x.User)
                .OrderByDescending(x => x.PostDate)
                .ToListAsync();

        }

        public async Task<IEnumerable<QuackEntity>> GetTweetsAndRetweetsFromUser(Guid userId) {
            return await _context.Quacks.Where(x => x.UserId == userId)
                        .OrderByDescending(x => x.PostDate)
                        .ToListAsync();
        }
    }
}
