﻿using QuackService.Dal.Entities;
using QuackService.Dal.Includers;
using QuackService.Dal.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace QuackService.Dal {
    public sealed class UnitOfWork : IUnitOfWork {

        public IQuackRepository QuackRepository { get; private set; }
        public IUserRepository UserRepository { get; private set; }
        public IFollowerRepository FollowerRepository { get; private set; }
        public IHashtagRepository HashtagRepository { get; private set; }

        private readonly QuackAppContext _context;

        public UnitOfWork(QuackAppContext context, ICollectionIncluder<IQueryable<UserEntity>, object> includer) {
            _context = context;
            QuackRepository = new QuackRepository(context);
            UserRepository = new UserRepository(context, includer);
            FollowerRepository = new FollowerRepository(context);
            HashtagRepository = new HashtagRepository(context);
        }

        public void Dispose() {
            _context.Dispose();
        }

        public async Task<int> Complete() {
            return await _context.SaveChangesAsync();
        }
    }
}
