﻿using Microsoft.EntityFrameworkCore;
using QuackService.Dal.Entities;

namespace QuackService.Dal {
    public class QuackAppContext : DbContext {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<QuackEntity> Quacks { get; set; }
        public DbSet<FollowEntity> Followers { get; set; }

        // Quack content
        public DbSet<HashtagEntity> Hashtags { get; set; }
        public DbSet<MentionEntity> Mentions { get; set; }

        public QuackAppContext(DbContextOptions options) : base(options) {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            OneUserHasManyQuacks(modelBuilder);
            FollowerEntityHasAComposedPrimaryKey(modelBuilder);
            OneUserHasManyFollowers(modelBuilder);
            OneUserHasManyFollowees(modelBuilder);
            OneUserHasManyMentions(modelBuilder);
        }

        private void OneUserHasManyQuacks(ModelBuilder modelBuilder) {
            modelBuilder.Entity<QuackEntity>()
            .HasOne(quack => quack.User)
            .WithMany(user => user.Quacks)
            .HasForeignKey(quack => quack.UserId);
        }

        private void FollowerEntityHasAComposedPrimaryKey(ModelBuilder modelBuilder) {
            modelBuilder.Entity<FollowEntity>().HasKey(fe => new { fe.UserId, fe.FollowerId });
        }

        private void OneUserHasManyFollowers(ModelBuilder modelBuilder) {
            modelBuilder.Entity<FollowEntity>()
                .HasOne(fe => fe.User)
                .WithMany(user => user.Followers)
                .HasForeignKey(fe => fe.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        }

        private void OneUserHasManyFollowees(ModelBuilder modelBuilder) {
            modelBuilder.Entity<FollowEntity>()
                .HasOne(fe => fe.Follower)
                .WithMany(user => user.Following)
                .HasForeignKey(fe => fe.FollowerId)
                .OnDelete(DeleteBehavior.NoAction);
        }


        private void OneUserHasManyMentions(ModelBuilder modelBuilder) {
            modelBuilder.Entity<MentionEntity>()
                .HasOne(mention => mention.User)
                .WithMany(user => user.Mentions)
                .HasForeignKey(mention => mention.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        }

    }
}
