﻿using QuackService.Dal.Repositories;
using System;
using System.Threading.Tasks;

namespace QuackService.Dal {
    /// <summary>
    /// Makes sure that all the repositories in this unit of work share the same database transaction
    /// This class can not be inherited to prevent complications with dispose.
    /// </summary>
    public interface IUnitOfWork : IDisposable {

        // IRepo's go here getters only
        IQuackRepository QuackRepository { get; }
        IUserRepository UserRepository { get; }
        IFollowerRepository FollowerRepository { get; }
        IHashtagRepository HashtagRepository { get; }

        /// <summary>
        /// Completes all the database operations and rolls back if anything fails
        /// </summary>
        /// <returns>Number of rows affected by the operations.</returns>
        Task<int> Complete();
    }
}
